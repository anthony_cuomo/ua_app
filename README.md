# UA App

# How to Run
1. Install [node](https://nodejs.org/en/download)
2. Open a terminal or command prompt
3. git clone <repo URL here>
4. cd/navigate to cloned repo directory
5. Run 'npm install'
6. Run 'npm start'
7. Open 'localhost:3000' in your browser
